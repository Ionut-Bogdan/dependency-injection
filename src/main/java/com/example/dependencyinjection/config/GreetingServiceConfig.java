package com.example.dependencyinjection.config;

import com.example.dependencyinjection.datasource.FakeDataSource;
import com.example.dependencyinjection.repositories.EnglishGreetingRepository;
import com.example.dependencyinjection.repositories.EnglishGreetingRepositoryImpl;
import com.example.dependencyinjection.services.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import ro.example.pets.PetService;
import ro.example.pets.PetServiceFactory;

@EnableConfigurationProperties(ConstructorConfig.class)
@Configuration
public class GreetingServiceConfig {

    @Bean
    public FakeDataSource fakeDataSource(ConstructorConfig constructorConfig) {
        FakeDataSource fakeDataSource = new FakeDataSource();
        fakeDataSource.setUserName(constructorConfig.getUserName());
        fakeDataSource.setPassword(constructorConfig.getPassword());
        fakeDataSource.setJdbcUrl(constructorConfig.getJdbcUrl());
        return fakeDataSource;
    }

    @Profile("cat")
    @Bean
    public PetService catPetService(PetServiceFactory petServiceFactory) {
        return petServiceFactory.getPetService("cat");
    }

    @Profile({"dog", "default"})
    @Bean
    public PetService dogPetService(PetServiceFactory petServiceFactory) {
        return petServiceFactory.getPetService("dog");
    }

    @Bean
    public PetServiceFactory petServiceFactory() {
        return new PetServiceFactory();
    }

    @Profile({"ES", "default"})
    @Bean("i18nService")
    public I18nSpanishGreetingService i18nSpanishGreetingService() {
        return new I18nSpanishGreetingService();
    }

    @Bean
    public EnglishGreetingRepository englishGreetingRepository() {
        return new EnglishGreetingRepositoryImpl();
    }

    @Profile("EN")
    @Bean
    public I18nEnglishGreetingService i18nService(EnglishGreetingRepository englishGreetingRepository) {
        return new I18nEnglishGreetingService(englishGreetingRepository);
    }

    @Primary
    @Bean
    public PrimaryGreetingService primaryGreetingService() {
        return new PrimaryGreetingService();
    }

    @Bean
    public ConstructorGreetingService constructorGreetingService() {
        return new ConstructorGreetingService();
    }

    @Bean
    public PropertyGreetingService propertyGreetingService() {
        return new PropertyGreetingService();
    }

    @Bean
    public SetterGreetingService setterGreetingService() {
        return new SetterGreetingService();
    }
}
