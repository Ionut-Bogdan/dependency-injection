package com.example.dependencyinjection;

import com.example.dependencyinjection.config.ConstructorConfig;
import com.example.dependencyinjection.config.PropertiesBindingConfiguration;
import com.example.dependencyinjection.controllers.*;
import com.example.dependencyinjection.datasource.FakeDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DependencyInjectionApplication {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(DependencyInjectionApplication.class, args);

        PetController petController = (PetController) ctx.getBean("petController");
        System.out.println("--- The Best Pet is ---");
        System.out.println(petController.witchPetIsTheBest());

        I18nController i18nController = (I18nController) ctx.getBean("i18nController");
        System.out.println(i18nController.sayHello());

        MyController myController = (MyController) ctx.getBean("myController");
        System.out.println("---------Primary Bean");
        System.out.println(myController.sayHello());

        System.out.println("---------Property");
        PropertyInjectedController propertyInjectedController = (PropertyInjectedController) ctx.getBean(
                "propertyInjectedController");
        System.out.println(propertyInjectedController.getGreeting());

        System.out.println("---------Setter");
        SetterInjectedController setterInjectedController = (SetterInjectedController) ctx.getBean(
                "setterInjectedController");
        System.out.println(setterInjectedController.getGreeting());

        System.out.println("---------Constructor");
        ConstructorInjectedController constructorInjectedController = (ConstructorInjectedController) ctx.getBean(
                "constructorInjectedController");
        System.out.println(constructorInjectedController.getGreeting());

        System.out.println("------------- Fake Data Source");
        FakeDataSource fakeDataSource = ctx.getBean(FakeDataSource.class);
        System.out.println(fakeDataSource.getUserName());
        System.out.println(fakeDataSource.getPassword());
        System.out.println(fakeDataSource.getJdbcUrl());

        System.out.println("------------- Config Props Bean");

        PropertiesBindingConfiguration propertiesBindingConfiguration =
                ctx.getBean(PropertiesBindingConfiguration.class);
        System.out.println(propertiesBindingConfiguration.getUserName());
        System.out.println(propertiesBindingConfiguration.getPassword());
        System.out.println(propertiesBindingConfiguration.getJdbcUrl());

        System.out.println("------------ Constructor Binding");

        ConstructorConfig constructorConfig = ctx.getBean(ConstructorConfig.class);
        System.out.println(constructorConfig.getUserName());
        System.out.println(constructorConfig.getPassword());
        System.out.println(constructorConfig.getJdbcUrl());
    }
}
